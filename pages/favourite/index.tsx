import Head from "next/head";
import { FavouriteDetails } from "../../src/interfaces/interfaces";
import { getAnimeList } from "../../src/store/API/AnimeList";
import { makeStore } from "../../src/store/configureStore";
import FavouriteContainer from "../../src/modules/Favourite/FavouriteContainer";

const Favourite = ({ dataList }: FavouriteDetails) => {
  return (
    <>
      <Head>
        <title>Favourite</title>
        <meta
          name="description"
          content="Hello there, This is Favourite Page"
        />
      </Head>
      <FavouriteContainer dataList={dataList} />
    </>
  );
};

export default Favourite;

export const getServerSideProps = async () => {
  const store = makeStore();
  const page = 1;
  const data = await store.dispatch(getAnimeList.initiate({ page }));

  return {
    props: {
      dataList: data?.data,
      data,
    },
  };
};
