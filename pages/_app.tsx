import * as React from "react";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import { AppProps } from "next/app";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider, EmotionCache } from "@emotion/react";
import theme from "../src/mui_imports/theme";
import createEmotionCache from "../src/mui_imports/createEmotionCache";
import store from "../src/store/configureStore";
import { Provider } from "react-redux";
import LayoutContainer from "../src/common/Layout/LayoutContainer";
import Loader from "../src/common/Loader/Loader";

const clientSideEmotionCache = createEmotionCache();

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

const MyApp = (props: MyAppProps) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  const [isLoading, setIsLoading] = useState(false);

  const router = useRouter();
  router.events?.on("routeChangeStart", (url) => {
    setIsLoading(true);
  });
  router.events?.on("routeChangeComplete", (url) => {
    setIsLoading(false);
  });

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>Main Page</title>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Provider store={store}>
          <LayoutContainer>
            {isLoading ? <Loader /> : <Component {...pageProps} />}
          </LayoutContainer>
        </Provider>
      </ThemeProvider>
    </CacheProvider>
  );
};
export default MyApp;
