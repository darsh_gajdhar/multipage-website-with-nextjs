import { LoaderWrapper } from "./style";
import CircularProgress from "@mui/material/CircularProgress";

const Loader = () => (
  <LoaderWrapper>
    <CircularProgress />
  </LoaderWrapper>
);

export default Loader;
