import { styled, Box } from "@mui/material";

export const LoaderWrapper = styled(Box, { name: "LoaderWrapper" })`
  display: flex;
  justify-content: center;
  height: 100vh;
  align-items: center;
`;
