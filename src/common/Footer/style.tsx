import { styled, Box } from "@mui/material";

export const FooterWrapper = styled(Box, {
  name: "FooterWrapper",
})`
  height: 70px;
  padding: 10px 0px;
  display: flex;
  justify-content: center;
  background-color: #212121;
  align-items: flex-end;
`;
